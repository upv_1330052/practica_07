package course.examples.practica_07;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    /*public void llamar(View view){
         EditText editText = (EditText) findViewById(R.id.phone);
         String numero = editText.getText().toString();
         Toast.makeText(this,"numero: "+numero, Toast.LENGTH_SHORT).show();
     }*/

    EditText num;

    public void llamada(View view) {
        try {
            EditText num = (EditText) findViewById(R.id.numero);
            String numero = num.getText().toString();

            if(numero.isEmpty() || numero.length()<10) {
                Toast.makeText(this, "INTRODUCE UN NUMERO VALIDO", Toast.LENGTH_SHORT).show();
            }else{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + numero));
                startActivity(callIntent);
            }

        } catch (ActivityNotFoundException activityException) {
            Log.e("dialing-example", "Call failed", activityException);
        }
    }

    public void mensaje(View view){
        EditText editText = (EditText) findViewById(R.id.numero);
        String numero = editText.getText().toString();
        EditText editText1 = (EditText) findViewById(R.id.texto);
        String mensaje = editText1.getText().toString();
        try {
            if(numero.isEmpty() || numero.length()<10){
                Toast.makeText(this,"INTRODUCE UN NUMERO VALIDO", Toast.LENGTH_SHORT).show();
            }else{
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(numero, null, mensaje, null, null);
                Toast.makeText(getApplicationContext(), "SMS ENVIADO", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "ERROR!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
